﻿using UnityEngine;
using Harmony;
using System.Collections;
using System;

namespace TestMod  // This is usually the name of your mod.
{
    [HarmonyPatch(typeof(PlayerComponent))]  // We're patching the PlayerComponent class.
    [HarmonyPatch("Update")]        // The PlayerComponent class's CheckEnergy method specifically.
    internal class PlayerComponent_Update_Patch
    {
        [HarmonyPrefix]      // Run this after the default game's PlayerComponent Update method runs.
        public static bool Prefix(PlayerComponent __instance)
        {
            MainGame.me.player.energy = (float)MainGame.me.save.max_energy;
            __instance.wgo.energy = (float)MainGame.me.save.max_energy;
            return true;
        }
    }
}
